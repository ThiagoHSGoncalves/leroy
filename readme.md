## Pré Requisitos
- LAMP (e vhost apontando para a pasta /public do projeto. No meu caso, defini o vhost: http://leroy.app)

- Gulp

- Bower

## Instalação
- Edite o arquivo .env (encontra-se na pasta raiz do projeto) com as credenciais de acesso ao banco de dados

- Abra um terminal, acesse a pasta do projeto e execute os comandos abaixo:

***composer install***

***php artisan migrate***

***php artisan db:seed***

- Acesse a pasta /resources do projeto e execute: ***bower install***

- Acesse a pasta raiz do do projeto e execute: ***gulp***  (para compilar os arquivos JS/CSS)


## Upload / Fila / Processamento dos Arquivos

Acesse a página de importação de produtos (no meu caso: http://leroy.app/produtos/novo) e faça o upload arquivo.

Após o upload do arquivo, é necessário enviá-los à fila de processamento.

Para isto, acesse: http://leroy.app/produtos/import

Este script é responsável por enviar todos os arquivos pendentes de processamento à fila.

Feito isso, execute o comando abaixo para iniciar o processamento (inclusão no banco de dados) dos registros: 

***php artisan queue:listen***