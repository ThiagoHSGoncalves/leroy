<?php 

namespace App;

use App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\Validator;

/**
* Model Produtos
*/
class Product extends Model
{
    protected $primaryKey = 'lm';
    protected $fillable = [
        'lm',
        'name',
        'free_shipping',
        'description',
        'price',
        'category'
    ];
    protected $rules = [
        'name' => 'required|max:100',
        'description' => 'required',
        'price' => 'required|numeric',
        'category' => 'required|max:50'
    ];

    /**
    * Pequenas validações baseadas nas regras acima
    */
    public function isValid()
    {
    	$validator = App::make(Validator::class);
    	$validator->setRules($this->rules);
    	$validator->setData($this->attributes);

    	if ($validator->fails()) {
    		$this->errors = $validator->errors();
    		return false;
    	}

    	return true;
    }
}
