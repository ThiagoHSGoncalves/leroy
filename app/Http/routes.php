<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function() {
    return view('welcome');
});



Route::group(['middleware' => ['web'], 'prefix' => 'produtos'], function()
{
	Route::get('', ['as' => 'products.index', 'uses' => 'ProductController@index']);
	Route::get('novo', ['as' => 'products.add', 'uses' => 'ProductController@add']);
	Route::get('editar/{id}', ['as' => 'products.edit', 'uses' => 'ProductController@edit']);
	Route::post('update/{id}', ['as' => 'products.update', 'uses' => 'ProductController@update']);
	Route::get('delete/{id}', ['as' => 'products.delete', 'uses' => 'ProductController@delete']);
	Route::get('import', ['as' => 'products.import', 'uses' => 'ProductController@importProductExcel']);
	Route::post('upload', ['as' => 'products.upload', 'uses' => 'ProductController@upload']);
});



/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});
