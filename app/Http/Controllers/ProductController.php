<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Leroy\Repositories\ProductRepository;
use Illuminate\Contracts\Routing\ResponseFactory;
use App\Leroy\Services\Validators\ProductValidator as Validator;
use Excel;
use App\Jobs\ImportProducts;

/**
* Responsável por gerenciar os produtos
*
* Responsável por efetuar o upload dos arquivos e fazer da devidas validações
* antes de inserir o registro na base.
* A partir dela é iniciado o serviço que enviará os registros pra fila
*/
class ProductController extends Controller
{
	protected $repo;
	protected $response;
	protected $validator;

	public function __construct(ProductRepository $productRepository, ResponseFactory $response, Validator $validator)
	{
		$this->repo = $productRepository;
		$this->response = $response;
		$this->validator = $validator;
	}

	/**
	* Lista os produtos do banco e carrega os dados na view
	*/
	public function index()
	{
		$products = $this->repo->all();
		return $this->response->view('products.index', compact('products'));
	}

	/**
	* Carrega a view de importação do arquivo XLSX
	*/
	public function add()
	{
		return $this->response->view('products.import');
	}

	/**
	* Faz algumas validações e salva o arquivo
	* O arquivo será processado pela Queue posteriormente
	* @todo Essa validação não deve ficar aqui
	*/
	public function upload()
	{
		// pego o que veio no input file do form
		$inputFile = \Request::file('file');

		// e verifico se ele realmente existe
		if (is_null($inputFile)) {
			return redirect()
				->back()
				->with('error', 'Cadê o arquivo? Não faço milagres.');	
		}

		// se veio, valido a extensão
		$ext = $inputFile->guessExtension();
		if ($ext !== 'xlsx') {
			return redirect()
				->back()
				->with('error', 'Arquivo/Extensão inválida.');
		}

		/** 
		* monto o nome do arquivo a ser salvo
		* apenas para manter um padrão e conseguir visualizar qual o mais recente
		* também vejo se o upload ocorreu conforme o esperado
		*/
		$fileName = (date('Ymd-His')).'.'.$ext;
		if (!$inputFile->isValid()) {
			return redirect()
				->back()
				->with('errors', $inputFile->getErrorMessage());
		}

		// uma vez que o upload rolou, mando o arquivo pra pasta correta
		$inputFile->move(storage_path('app/import'), $fileName);

		// redireciono para a view com a mensagem de sucesso
		return redirect()
				->route('products.index')
				->with('success', 'O arquivo adicionado à fila e será processado em breve.');
	}

	/**
	* Dispara a Queue responsável por varrer a pasta e importar todos os arquivos que estiverem lá
	*/
	public function importProductExcel()
	{
		$this->dispatch(new ImportProducts());
		return redirect()
			->route('products.index')
			->with('success', 'Job iniciado.');
	}

	/**
	* Carrego o a view que contém o form de edição com os dados do banco
	*/
	public function edit($id)
	{
		$product = $this->repo->find($id);
		if (empty($product)) {
			return redirect()
				->route('products.index')
				->with('error', 'Este produto não existe.');	
		}

		return $this->response->view('products.edit', compact('product'));
	}

	/**
	* Valido os dados que foram preenchidos no form e 
	* caso tudo esteja OK, gravo no banco
	*/
	public function update($id)
	{
		$formData = \Request::all();

		if (!$this->validator->passes()) {
			return redirect()
				->back()
				->with('errors', $this->validator->getErrors())->withInput();
		}

		$this->repo->update($formData, $id);

		return redirect()
			->back()
			->with('success', 'Produto atualizado com sucesso!');
	}

	/**
	* O próprio nome já diz
	* Apaga um produto do banco de dados
	*/
	public function delete($id)
	{
		$response = $this->repo->delete($id);

		if (is_null($response)) {
			return redirect()
				->route('products.index')
				->with('error', 'Produto não localizado');
		}

		if ($response) {
			return redirect()
				->route('products.index')
				->with('success', 'Produto excluido com sucesso!');
		}

		return redirect()
			->route('products.index')
			->with('error', 'Erro ao excluir Produto');
	}
}
