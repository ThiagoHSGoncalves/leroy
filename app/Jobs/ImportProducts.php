<?php 

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Product;
use Exception;
use App\Leroy\Services\Validators\ProductValidator as Validator;
use File as file;
use PHPExcel_IOFactory as phpExcel;

class ImportProducts extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    // pasta para onde o upload é feito
    private $path;
    // pasta onde aloco os arquivos que estão sendo processados
    private $inProgressPath;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->path = storage_path('app/import/');
        $this->inProgressPath = storage_path('app/import_inprogress/');
    }

    /**
    * List files from path
    */
    public function getFiles($path)
    {
        if (!isset($path) || !is_dir($path)) {
            return false;
        }

        $files = file::allFiles($path);

        // empty? return null
        if (empty($files)) {
            return null;
        }

        return $files;
    }

    public function loadWorksheet($filePath)
    {
        if (!file_exists($filePath)) {
            return false;
        }

        $objPHPExcel    = phpExcel::load($filePath);
        $objWorksheet   = $objPHPExcel->getActiveSheet();
        $arrData        = $objWorksheet->toArray(null, true, true, true);
        $category       = $objWorksheet->getCell('B2')->getValue();
        unset($arrData[1], $arrData[2], $arrData[3], $arrData[4]);

        if (empty($arrData) || $category === '') {
            return null;
        }

        return [
            'category'  => $category,
            'data'      => $arrData
        ];
    }

    /**
     * Execute the job.
     * @return void
     */
    public function handle()
    {
        \DB::beginTransaction();
        try {
            $filesList = $this->getFiles($this->path);

            if ($filesList === null) {
                return true;
            }

            if (!is_array($filesList)) {
                throw new Exception("Erro ao listar arquivos.");
            }

            // itero todos os arquivos da pasta
            foreach ($filesList as $excel) {
                $name = $excel->getRelativePathName();
                file::move($this->path.$name, $this->inProgressPath.$name);

                $worksheet  = $this->loadWorksheet($this->inProgressPath.$name);
                if (is_null($worksheet)) {
                    continue;
                }

                if (!is_array($worksheet['data']) || trim($worksheet['category']) === '') {
                    throw new Exception("Arquivo vazio ou em formato incorreto.");
                }

                $category   = $worksheet['category'];
                $arrData    = $worksheet['data'];

                $ret = [];

                foreach ($arrData as $line => $column) {
                    /** 
                    * as vezes ele pega umas linhas vazias então quando cair nelas, posso ir para a próxima
                    * poderia usar o break mas vai que alguém pula uma linha no arquivo
                    * então melhor pular para a proxima linha
                    */
                    if(is_null($column['A'])) {
                        continue;
                    }

                    $ret = [
                        'lm'            => $column['A'],
                        'category'      => $category,
                        'name'          => $column['B'],
                        'free_shipping' => $column['C'],
                        'description'   => $column['D'],
                        'price'         => $column['E']
                    ];

                    // if exists, update
                    if (!Product::updateOrCreate(['lm'=>$column['A']], $ret)) {
                        throw new Exception("Erro ao executar update/insert na base.");
                    } 
                }
            }

            \DB::commit();
            file::delete($this->inProgressPath.$name);
            return true;

        } catch (Exception $e) {
            \DB::rollback();
            return false;
        }
    }
}
