<?php

namespace App\Leroy\Services\Validators;

/**
* Regra de validação
*/
class ProductValidator extends Validator
{
  	public static $rules = [
	    'name' => 'required|max:100',
	    'description' => 'required',
	    'free_shipping' => 'required|integer',
	    'price' => 'required|numeric',
	    'category' => 'required|max:50'
	];

	public static $messages = [
		'name.required' => 'O campo Nome do produto é obrigatório!',
		'name.max' => 'O campo Nome do produto deve conter no máximo 100 caracteres!',
		'description.required' => 'O campo Descrição do produto é obrigatório!',
		'free_shipping.required' => 'O campo Frete Grátis é obrigatório!',
		'price.required' => 'O campo Preço do produto é obrigatório!',
		'price.numeric' => 'O Preço informado é inválido!',
		'category.required' => 'O campo Categoria do produto é obrigatório!',
		'category.max' => 'O campo Categoria do produto deve conter no máximo 50 caracteres!'
	];
}
