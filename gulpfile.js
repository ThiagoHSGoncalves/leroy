var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.scripts([
        '../../bower_components/jquery/dist/jquery.js',
        '../../bower_components/jquery-ui/jquery-ui.js',
        '../../js/modernizr.custom.js',
        '../../bower_components/jquery-validation/dist/jquery.validate.js',
        '../../bower_components/screenfull/dist/screenfull.js',
        '../../js/powerwidgets.js',
        '../../bower_components/raphael/raphael.js',
        '../../bower_components/bootstrap/dist/js/bootstrap.js',
        '../../bower_components/metisMenu/dist/metisMenu.js',
        '../../bower_components/nanoscroller/bin/javascripts/jquery.nanoscroller.js',
        '../../bower_components/datatables/media/js/jquery.dataTables.js',
        '../../bower_components/datatables-responsive/js/dataTables.responsive.js',
        '../../bower_components/typeahead.js/dist/typeahead.jquery.js',
        '../../js/jquery.maskMoney.min.js',
        '../../js/scripts.js',
    ], 'public/js/leroy.js');

    mix.styles([
        '../../bower_components/entypo/font/entypo.css',
        '../../bower_components/font-awesome/css/font-awesome.css',
        '../../bower_components/datatables/media/css/jquery.dataTables.css',
        '../../bower_components/datatables-responsive/css/responsive.dataTables.scss',
        '../../bower_components/nanoscroller/bin/css/nanoscroller.css',
        '../../css/animate.css',
        '../../bower_components/bootstrap/dist/css/bootstrap.css',
        '../../bower_components/metisMenu/dist/metisMenu.min.css',
        '../../css/styles.css',
    ], 'public/css/leroy.css');

    /* Gera a versão dos arquivos, para evitar problemas de cache */
    mix.version([
        'public/css/leroy.css',
        'public/js/leroy.js'
    ]);
});