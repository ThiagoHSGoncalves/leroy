<?php 

namespace App\Jobs;

use Mockery as m;
use App;
use TestCase;

/**
* Testes Produtos
*/
class ImportProductsJobTest extends TestCase
{

	public function tearDown()
	{
		parent::tearDown();
		m::close();
	}

	/** 
	* @test 
	* Testa se o path informado é um diretório
	*/
	public function should_must_be_said_when_path_is_not_a_valid_directory()
	{
		$job = new ImportProducts;
		$this->assertFalse(
			$job->getFiles('')
		);
	}

	/** 
	* @test 
	* Testa se o arquivo informado existe
	*/
	public function should_must_be_said_when_file_not_exists()
	{
		$job = new ImportProducts;
		$this->assertFalse(
			$job->loadWorksheet('')
		);
	}

	/** 
	* @test 
	* Testa se o arquivo é válido (possui registros)
	*/
	public function should_must_be_said_when_file_is_valid()
	{
		$job = new ImportProducts;
		$this->assertEquals(
			[
				"category" => "tecnologia", 
				"data" => [
					5 => [
						"A" => 1.0,
						"B" => "teste",
						"C" => 0.0,
						"D" => "teste",
						"E" => 0.0
					]
				]
			],
			$job->loadWorksheet(storage_path('app/tests/produtos.xlsx'))
		);
	}

	/** 
	* @test 
	* Testa se o arquivo está vazio
	*/
	public function should_must_be_said_when_file_is_empty()
	{
		$job = new ImportProducts;
		$this->assertEquals(
			null,
			$job->loadWorksheet(storage_path('app/tests/produtos-empty.xlsx'))
		);
	}
}
