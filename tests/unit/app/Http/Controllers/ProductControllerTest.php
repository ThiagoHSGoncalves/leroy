<?php 

namespace App\Http\Controllers;

use Mockery as m;
use Leroy\Repositories\ProductRepository;
use App;
use TestCase;
use Illuminate\Contracts\Routing\ResponseFactory;
use App\Leroy\Services\Validators\ProductValidator as Validator;
use App\Http\Requests;

/**
* Testes Produtos
*/
class ProductControllerTest extends TestCase
{

	public function tearDown()
	{
		parent::tearDown();
		m::close();
	}

	/** 
	* @test 
	* Carrega a view com todos os produtos
	*/
	public function should_load_view_with_all_products()
	{
		// set
		$repo = m::mock(ProductRepository::class);
		$responseFactory = m::mock(ResponseFactory::class);
		$validator = m::mock(Validator::class);
		$controller = new ProductController($repo, $responseFactory, $validator);
		$products = [];
		$htmlResponse = m::mock();

		// expect
		$repo->shouldReceive('all')
			->once()
			->andReturn($products);

		$responseFactory->shouldReceive('view')
			->once()
			->with('products.index', ['products' => $products])
			->andReturn($htmlResponse);

		// asserts
		$this->assertEquals(
			$htmlResponse,
			$controller->index()
		);
	}

	/** 
	* @test 
	* Ao tentar acessar a página de adição de produtos (importaçao excel)
	* garante que a view carregada é a correta
	*/
	public function should_load_view_add()
	{
		// set
		$repo = m::mock(ProductRepository::class);
		$responseFactory = m::mock(ResponseFactory::class);
		$validator = m::mock(Validator::class);
		$controller = new ProductController($repo, $responseFactory, $validator);
		$htmlResponse = m::mock();

		// expect
		$responseFactory->shouldReceive('view')
			->once()
			->with('products.import')
			->andReturn($htmlResponse);

		// asserts
		$this->assertEquals(
			$htmlResponse,
			$controller->add()
		);
	}


}
