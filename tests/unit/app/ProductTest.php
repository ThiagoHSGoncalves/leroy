<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Mockery as m;
use App\Product;
use Illuminate\Validation\Validator;

/**
* Testes Produtos
*/
class ProductTest extends TestCase
{

    public function tearDown()
    {
        parent::tearDown();
        m::close();
    }

    /** 
    * @test 
    * Garante que é uma instancia do eloquent
    */
    public function should_be_eloquent_model_instance()
    {
        // Set
        $product = new Product;

        // Assert
        $this->assertInstanceOf(
            'Illuminate\Database\Eloquent\Model',
            $product
        );
    }

    /** 
    * @test 
    * Deve dizer que é válido (quando realmente for)
    */
    public function should_must_be_said_that_it_is_when_really_is()
    {
        // Set
        $product = new Product;
        $validator = m::mock(Validator::class);
        $rules = [
            'name' => 'required|max:100',
            'description' => 'required',
            'price' => 'required|numeric',
            'category' => 'required|max:50'
        ];

        $product->name = 'xpto';

        // Expect
        App::instance(Validator::class, $validator);

        $validator->shouldReceive('setRules')
            ->once()
            ->with($rules);

        $validator->shouldReceive('setData')
            ->once()
            ->with(['name' => 'xpto']);
        
        $validator->shouldReceive('fails')
            ->once()
            ->andReturn(false);

        // Assert
        $this->assertTrue(
            $product->isValid()
        );
    }

    /** 
    * @test 
    * Deve duzer que é inválido (quando for) e exibe os erros
    */
    public function should_be_said_when_is_invalid_and_display_errors()
    {
        // Set
        $product = new Product;
        $validator = m::mock(Validator::class);
        $errorMessageBag = m::mock('Illuminate\Support\MessageBag');
        $rules = [
            'name'          => 'required|max:100',
            'description'   => 'required',
            'price'         => 'required|numeric',
            'category'      => 'required|max:50'
        ];

        $product->name = 'xpto';

        // Expect
        App::instance(Validator::class, $validator);

        $validator->shouldReceive('setRules')
            ->once()
            ->with($rules);

        $validator->shouldReceive('setData')
            ->once()
            ->with(['name' => 'xpto']);
        
        $validator->shouldReceive('fails')
            ->once()
            ->andReturn(true);

        $validator->shouldReceive('errors')
            ->once()
            ->andReturn($errorMessageBag);

        // Assert
        $this->assertFalse(
            $product->isValid()
        );

        $this->assertEquals(
            $errorMessageBag,
            $product->errors
        );
    }
}
