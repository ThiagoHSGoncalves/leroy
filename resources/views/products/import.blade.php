@extends('layouts.main')

@section('content')

<!-- Sobreescrevo alguns estilos do Dante 2 -->
<div class="breadcrumb clearfix">
    <ul>
       <li><a href="/"><i class="fa fa-home"></i></a></li>
       <li><a href="{!! route('products.index') !!}">Produtos</a></li>
       <li class="activeCinza"><a href="javascript:void(0)">Importar</a></li>
    </ul>
</div>

@include('partials.alerts')

<div class="row">
    <div class="col-md-12">
        {!! Form::open(['route'=>'products.upload', 'files'=>true, 'class'=>'orb-form']) !!}
		<section>
			{!! Form::label('file', 'Arquivo XLSX', array('class' => 'label')) !!}
			<label class="input">
				{!! Form::file('file') !!}
			</label>
		</section>
        <section>
          {!! Form::submit('Importar', ['class'=>'btn btn-primary']) !!}
        </section>

        {!! Form::close() !!}
    </div>
</div>

@stop