@extends('layouts.main')

@section('content')

<!-- Sobreescrevo alguns estilos do Dante 2 -->
<div class="breadcrumb clearfix">
    <ul>
       <li><a href="/"><i class="fa fa-home"></i></a></li>
       <li><a href="{!! route('products.index') !!}">Produtos</a></li>
       <li class="activeCinza"><a href="javascript:void(0)">Editar</a></li>
    </ul>
</div>

@include('partials.alerts')

<div class="row">
    <div class="col-md-12">
        {!! Form::model($product, ['route' => ['products.update', $product->lm], 'class'=>'orb-form']) !!}

        @include('partials.products.form', ['action'=>'edit'])

        <section>
          {!! Form::submit('Salvar', ['class'=>'btn btn-primary', 'id'=>'save-product']) !!}
        </section>

        {!! Form::close() !!}
    </div>
</div>


@stop