<!DOCTYPE html>

<head>
  <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Teste Vaga Backend - Leroy</title>
  <link rel="stylesheet" href="{{ elixir("css/leroy.css") }}">
</head>

<body>

<!--Smooth Scroll-->
<div class="smooth-overflow"> 
  <!--Navigation-->
  <nav class="main-header clearfix" role="navigation"> 
    <a class="navbar-brand" href="/">
      <span class="text-blue">BACKEND</span>
    </a>
    
    <!--Navigation Itself-->
    
    <div class="navbar-content"> 
      <!--Sidebar Toggler--> 
      <a href="#" class="btn btn-default left-toggler"><i class="fa fa-bars"></i></a> 
      <!--Right Userbar Toggler--> 
      
      <!--Fullscreen Trigger-->
      <button type="button" class="btn btn-default hidden-xs pull-right" id="toggle-fullscreen"> <i class=" icon-popup"></i> </button>
  </nav>
  <!--/Navigation--> 
  
  <!--MainWrapper-->
  <div class="main-wrap"> 
    <!--Main Menu-->
    <div class="responsive-admin-menu">
      <div class="responsive-menu">Leroy
        <div class="menuicon"><i class="fa fa-angle-down"></i></div>
      </div>
      <ul id="menu">
        <li>
          <a class="tooltiped active" href="{!! route('products.index') !!}">
            <span class="font11"><i class="fa fa-home"></i>Produtos</span>
          </a>
        </li>
      </ul>
    </div>
    <!--/MainMenu-->
    
    <div class="content-wrapper"> 
      
      <!-- Aqui é o conteúdo da View -->
      @yield('content')
       
    </div>
    <!-- / Content Wrapper --> 
  </div>
  <!--/MainWrapper--> 
</div>
<!--/Smooth Scroll--> 

<!-- scroll top -->
<div class="scroll-top-wrapper hidden-xs"> <i class="fa fa-angle-up"></i> </div>
<!-- /scroll top --> 

<!--Scripts--> 
<script src="{{ elixir("js/leroy.js") }}"></script>
<!--/Scripts-->

</body>

</html>