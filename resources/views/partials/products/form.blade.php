<fieldset>
	<section>
		{!! Form::label('lm', 'ID', array('class' => 'label')) !!}
		<label class="input">
			{!! Form::text('lm', null, ['disabled' => 'disabled']) !!}
		</label>
	</section>
	<section>
		{!! Form::label('category', 'Categoria', array('class' => 'label')) !!}
		<label class="input">
			{!! Form::text('category', null, ['maxlength'=>'50']) !!}
		</label>
	</section>
	<section>
		{!! Form::label('name', 'Nome', array('class' => 'label')) !!}
		<label class="input">
			{!! Form::text('name') !!}
		</label>
	</section>
	<section>
		{!! Form::label('description', 'Descrição', array('class' => 'label')) !!}
		<label class="input">
			{!! Form::textarea('description') !!}
		</label>
	</section>
	<section>
		{!! Form::label('price', 'Preço', array('class' => 'label')) !!}
		<label class="input">
			{!! Form::text('price') !!}
		</label>
	</section>
	<section>
		{!! Form::label('free_shipping', 'Frete Grátis?', array('class' => 'label')) !!}
		<label class="select2">
			{!! Form::select('free_shipping', [1 => 'SIM', 0 => 'NÃO']) !!}
			<i></i>
		</label>
	</section>
</fieldset>