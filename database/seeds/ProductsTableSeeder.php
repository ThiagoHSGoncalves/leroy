<?php

use Illuminate\Database\Seeder;
use App\Product;
use Faker\Generator as faker;

class ProductsTableSeeder extends Seeder 
{
	
	private $f;
	private $p;

	public function __construct(faker $faker, Product $product)
	{
		$this->f = $faker;
		$this->p = $product;
	}

    public function run()
    {
        DB::table('products')->truncate();

        foreach (range(1,10) as $i) {
	        $this->p->create([
	        	'name' 			=> $this->f->word(),
	        	'free_shipping' => $this->f->boolean(),
	        	'description' 	=> $this->f->sentence(),
	        	'price' 		=> $this->f->randomFloat(),
	        	'category' 		=> 'Ferramentas'
	    	]);
        }
    }
}
